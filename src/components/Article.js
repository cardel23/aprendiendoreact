import React, { Component, Fragment } from "react";
import Sidebar from "./Sidebar";
import Global from "../Global";
import Axios from "axios";
import Error404 from "./Error";
import Moment from "react-moment";
import ImageDefault from '../assets/images/noImage.jpg';
import { Redirect, Link } from "react-router-dom";
import swal from "sweetalert";


class Article extends Component {

    url = Global.url;

    state = {
        article: null,
        status: null
    };

    getArticle = () => {
        var id = this.props.match.params.id;
        Axios.get(this.url + 'article/' + id)
            .then(res => {
                this.setState({
                    article: res.data.article,
                    status: res.data.status
                })
            })
            // Para que se muestre la pagina de cargando...
            .catch(err => {
                console.log(err);
                this.setState({
                    article: null,
                    status: "success"
                })
            });
    }

    componentDidMount() {
        this.getArticle();
    }

    deleteArticle = (id) => {
        // alert(id);
        swal({
            title: "Are you sure?",
            text: "Once deleted, you will not be able to recover this imaginary post!",
            icon: "info",
            buttons: [true, true], //poner el array de los botones con boolean
            dangerMode: true,
        })
            .then((willDelete) => {
                if (willDelete) {
                    Axios.delete(this.url + 'article/' + id)
                        .then(res => {
                            this.setState({
                                article: res.data.article,
                                status: "deleted"
                            })

                            swal("Poof! Your imaginary post has been deleted!", {
                                icon: "success",
                            });
                        },
                            error => {
                                console.log(error);
                                swal("Your imaginary post has not been deleted!", {
                                    icon: "info",
                                });
                            }
                        );
                } else {
                    swal("Your imaginary post is safe!");
                }
            });

    }

    render() {
        var article = this.state.article;
        // console.log(article)
        // if (article === undefined) {
        //     return (
        //         <Error404 />
        //     )
        // } else {

        if (this.state.status === 'deleted') {
            return (<Redirect to="/blog" />)
        }
        return (


            <Fragment>
                <div id="main">

                    {
                        article &&
                        <div className="center">
                            <section id="content">

                                <div id="articles">
                                    <article className="article-item article-detail" id="article-template">
                                        <div className="image-wrap">
                                            {
                                                article.image !== null ? (
                                                    <img src={this.url + 'get-image/' + article.image} alt={article.title} />
                                                ) :
                                                    (
                                                        <img src={ImageDefault} alt={article.title} />
                                                    )
                                            }

                                        </div>
                                        <h1 className="subheader">
                                            {this.state.article.title}
                                        </h1>
                                        <span className="date">
                                            <Moment fromNow>{article.date}</Moment>
                                        </span>
                                        <p>
                                            {article.content}
                                        </p>
                                        <Link to={'/blog/article/edit/'+article._id} className="btn btn-warning">Editar</Link>
                                        <button onClick={
                                            () => {
                                                this.deleteArticle(article._id)
                                            }
                                        } className="btn btn-danger">Borrar</button>

                                        <div className="clearfix"></div>
                                    </article>
                                </div>

                            </section>

                            <Sidebar />
                        </div>

                    }
                    {!article && this.state.status === "success" &&
                        // ) : (
                        <Error404 />
                        // )
                    }
                    {
                        this.state.status === null &&
                        <div className="center">
                            <section id="content">
                                <h2 className="subheader">Cargando ...</h2>
                            </section>
                            <Sidebar />
                        </div>
                    }
                </div>
            </Fragment>
        );
    }
    // }
}

export default Article;