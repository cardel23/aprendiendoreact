import React, { Component } from "react";
import axios from 'axios'
import Global from "../Global";

import Moment from 'react-moment';
import 'moment/locale/es';

import ImageDefault from '../assets/images/noImage.jpg';
import { Link } from "react-router-dom";
class Articles extends Component {

    state = {
        articles: [],
        status: null
    };

    url = Global.url;

    componentDidMount() {
        // this.getArticles();
        var home = this.props.home;
        var search = this.props.search;
        if (home === 'true') {
            this.getLastArticles()
        } else if (search && search !== null && search !== undefined) {
            this.search(search)
        } else {
            this.getArticles()
        }

    }


    getLastArticles = () => {
        // console.log("Articulos");

        axios.get(this.url + "articles/5")
            .then(res => {
                var data = res.data;
                // console.log(res.data);
                this.setState({
                    articles: data.articles,
                    status: "success"
                });
                // console.log(this.state);
            });

    }

    search = (param) => {
        // console.log("Articulos");

        axios.get(this.url + "search/" + param)
            .then(res => {
                var data = res.data;

                if (data.articles)
                    this.setState({
                        articles: data.articles,
                        status: "success"
                    });
                    console.log(this.state);
            })
            .catch( error =>{
                this.setState({
                    articles: [],
                    status: "success"
                });
                // console.log(error);
            });


    }

    getArticles = () => {
        // console.log("Articulos");

        axios.get(this.url + "articles")
            .then(res => {
                var data = res.data;
                // console.log(res.data);
                this.setState({
                    articles: data.articles,
                    status: "success"
                });
                // console.log(this.state);
            });

    }

    render() {
        if (this.state.articles.length > 0) {
            return (
                this.state.articles.map((article, i) => {
                    return (

                        <article key={article._id} className="article-item" id="article-template">
                            <div className="image-wrap">
                                {
                                    article.image !== null ? (
                                        <img src={this.url + 'get-image/' + article.image} alt={article.title} />
                                    ) :
                                        (
                                            <img src={ImageDefault} alt={article.title} />
                                        )
                                }
                                {/* <img src={this.url + 'get-image/' + article.image} alt={article.title} /> */}

                            </div>
                            <h2 >
                                {article.title}
                            </h2>
                            <span className="date">
                                <Moment locale="es" fromNow>{article.date}</Moment>
                            </span>
                            {/*                             <NavLink to={"/article/"+article._id}>Leer mas</NavLink>
 */}                            <Link to={"/blog/article/" + article._id}>Leer mas</Link>
                            <div className="clearfix"></div>
                        </article>
                    )

                })
            );
        } else if (this.state.articles.length === 0 && this.state.status === "success") {
            return (
                <div id="articles">
                    <h2 className="">No hay nada o k ase</h2>

                </div>
            )
        } else {
            return (
                <div id="articles">
                    <h1 className="">Carganding o k ase...</h1>

                </div>
            )
        }

    }
}

export default Articles;