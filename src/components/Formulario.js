import React, { Component } from "react";
import Sidebar from "./Sidebar";

class Formulario extends Component {

    nombreRef = React.createRef();
    apellidoRef = React.createRef();
    bioRef = React.createRef();
    hombreRef = React.createRef();
    mujerRef = React.createRef();
    otroRef = React.createRef();


    // state = {
    //     user: {}
    // };

    constructor(props){
        super(props);
        this.state = {
            selectedOption: null,
            user: {}
        };
    }


    recibirFormulario = (e) => {

        e.preventDefault(); //Para que no se cargue de nuevo la pantalla con los valores del formulario
        // alert();

        var genero;

        if (this.hombreRef.current.checked) {
            genero = this.hombreRef.current.value;
        } else if (this.mujerRef.current.checked) {
            genero = this.mujerRef.current.value;
        } else {
            genero = this.otroRef.current.value;
        }

        var user = {
            nombre: this.nombreRef.current.value,
            apellido: this.apellidoRef.current.value,
            bio: this.bioRef.current.value,
            genero: genero,

        }

        this.setState({
            user: user
        });
        console.log(user);
    }

    handleOptionChange = (event) => {
        this.setState({
            selectedOption: event.target.value
        });
    };

    render() {

        var user = this.state.user;

        return (
            <div id="formulario">

                {/* <Slider
                    title="Formulario"
                /> */}
                <div className="center">

                    <section id="content">
                        <h1 className="subheader">Formulario</h1>

                        {
                            this.state.user.nombre &&
                            <div id="user-data">
                                <p>Nombre: <strong>{user.nombre}</strong></p>
                                <p>Apellido: <strong>{user.apellido}</strong></p>
                                <p>Bio: <strong>{user.bio}</strong></p>
                                <p>Sexo: <strong>{user.genero}</strong></p>
                            </div>
                        }


                        <form className="mid-form" onSubmit={this.recibirFormulario} onChange={this.recibirFormulario}>
                            <div className="form-group">
                                <label htmlFor="nombre">Nombre</label>
                                <input type="text" name="nombre" id="" required ref={this.nombreRef} />
                            </div>
                            <div className="form-group">
                                <label htmlFor="apellido">Apellido</label>
                                <input type="text" name="apellido" required id="" ref={this.apellidoRef} />
                            </div>
                            <div className="form-group">
                                <label htmlFor="bio">Biografia</label>
                                <textarea name="bio" id="" required ref={this.bioRef}></textarea>
                            </div>
                            <div className="form-group radiobuttons">
                                <input type="radio" name="genero" value="hombre" id="" required ref={this.hombreRef} />Hombre
                                <input type="radio" name="genero" value="mujer" id="" onChange={this.handleOptionChange} required ref={this.mujerRef} />Mujer
                                <input type="radio" name="genero" value="otro" id="" onChange={this.handleOptionChange} required ref={this.otroRef} />Otro
                            </div>
                            <div className="clearfix"></div>
                            <input type="submit" value="Enviar" className="btn btn-success" />
                        </form>
                    </section>
                    <Sidebar />

                    <div className="clearfix"></div>
                </div>
            </div >
        )

    }

}

export default Formulario;