import React, { Component } from "react";
import Slider from "./Slider";
import Sidebar from "./Sidebar";
import Articles from "./Articles";

class Home extends Component {

    render() {

        var buttonString = "Ir al blog";

        return (
            <div id="main">
                {/* Pasar parametros al componente para acceder mediante this.props */}
                <Slider
                    title="Bienvenido al master en frameworks para JS"
                    size="slider-big"
                    btn={buttonString} />
                <div className="center">
                    <section id="content">
                        <h1 className="subheader">Últimos artículos</h1>
                        <Articles
                            home='true'
                        />
                    </section>
                    <Sidebar />

                    <div className="clearfix"></div>
                </div>
            </div>
        );
    }
}

export default Home;