import React, { Component, Fragment } from 'react';
import Pelicula from './Pelicula';
import Sidebar from './Sidebar';
import Slider from './Slider';

class Peliculas extends Component {

    state = {

    }

    cambiarTitulo = () => {
        var { peliculas } = this.state;
        peliculas[0].title = "Ultraviolenta"
        this.setState({
            peliculas: peliculas
        })
    }

    fav = (pelicula, index) => {
        console.log(pelicula, index);

        this.setState({
            favorita: pelicula
        });
    }

    // antes de montar el componente
    componentWillMount() {
        // alert("Se va a montar el componente");
        this.setState(
            {
                peliculas: [
                    { year: 2017, title: "Fantasma en la Concha", image: "https://cdn2.actitudfem.com/media/files/styles/big_img/public/images/2017/03/ghost.jpg" },
                    { year: 2019, title: "Los Avengers", image: "https://bombanoise.com/wp-content/uploads/2019/02/avengers-endgame-heroes-in-war-wallpaper-800x600-15969_17-1.jpg" },
                    { year: 2019, title: "El Bromas", image: "https://images.wallpapersden.com/image/download/2019-joker-movie-4k_66970_800x600.jpg" }
                ],
                nombre: "Carlos",
                favorita: {}
            }
        );
    }

    // despues de montar el componente
    componentDidMount() {
        // alert("Ya se montó");
    }


    componentWillUnmount() {

    }

    render() {

        var pStyle = {
            background: 'green',
            color: 'white',
            padding: '10px'
        };

        var favorita;
        if (this.state.favorita.title) {
            favorita = (
                <p className="favorita" style={pStyle}>
                    <strong>La película favorita es: </strong>
                    <span>{this.state.favorita.title}</span>
                </p>
            )
        } else {
            favorita = (<p>No hay favorita</p>)
        }

        return (
            <Fragment>
                <div id="main">

                    <Slider
                        title="Películas"
                        size="slider-small" />
                    <div className="center">
                        <section id="content" className="peliculas">

                            <h2 className="subheader">Selección de películas bergas de {this.state.nombre}</h2>
                            {/* <p>Selección de películas bergas de {this.state.nombre}</p> */}
                            <div>
                                <button onClick={this.cambiarTitulo}>Cambiar titulo</button>
                            </div>

                            {
                                // Condicional similar a ngIf de angular
                                // [[expresion]] && codigo

                                /*
                                this.state.favorita.title &&
                                <p className="favorita" style={pStyle}>
                                    <strong>La película favorita es: </strong>
                                    <span>{this.state.favorita.title}</span>
                                </p>
                                */

                                // Condicional ternaria estilo java para if-else

                                /*
                                this.state.favorita.title ?(
                                <p className="favorita" style={pStyle}>
                                    <strong>La película favorita es: </strong>
                                    <span>{this.state.favorita.title}</span>
                                </p>
                                ) : (
                                    <p>No hay favorita</p>
                                )
                                */

                                // Condicional definida con JS
                                favorita
                            }

                            {/* Componente pelicula */}

                            <div id="articles" className="pelicula">
                                {
                                    this.state.peliculas.map((pelicula, i) => {
                                        return (
                                            <Pelicula
                                                key={i}
                                                index={i}
                                                pelicula={pelicula}
                                                fav={this.fav} />
                                        )
                                    })
                                }
                            </div>
                            <div className="clearfix"></div>
                        </section>

                        <Sidebar />

                        <div className="clearfix"></div>
                    </div>
                </div>

            </Fragment>

        );
    }

}

export default Peliculas;