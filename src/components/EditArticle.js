import React, { Component } from "react";
import Axios from "axios";
import Global from "../Global";
import Sidebar from "./Sidebar";
import { Redirect } from "react-router-dom";
import SimpleReactValidator from 'simple-react-validator';
import Swal from 'sweetalert';
import ImageDefault from '../assets/images/noImage.jpg';


class EditArticle extends Component {

    url = Global.url;

    titleRef = React.createRef();
    contentRef = React.createRef();
    fileRef = React.createRef();

    state = {
        article: {},
        status: null,
        selectedFile: null
    };

    constructor(props) {
        super(props);
        this.articleId = props.match.params.id;
        this.validator = new SimpleReactValidator({
            // errores para cada campo segun la condicion
            messages: {
                required: "Este campo es requerido",
                alpha_num_space: "El título no puede tener caracteres especiales"
            }
        });
    }

    getArticle = (id) => {
        Axios.get(this.url + "article/" + id)
            .then(res => {
                this.setState({
                    article: res.data.article,
                });
                console.log(this.state);
            })
    }

    changeState = () => {
        this.setState({
            article: {
                title: this.titleRef.current.value,
                content: this.contentRef.current.value,
                image: this.state.article.image
            }
        });
        this.validator.showMessages();
        this.forceUpdate();
        // console.log(this.state.article);
    };

    componentDidMount() {
        this.getArticle(this.articleId);
        // console.log(this.state);

    }

    // NO PONER PARENTESIS AL SUBMIT SINO NO FUNCIONA 
    saveArticle = (e) => {
        e.preventDefault();
        this.changeState();
        if (this.validator.allValid()) {
            Axios.put(this.url + "article/"+this.articleId, this.state.article)
                .then(res => {
                    console.log(res.data);
                    if (res.data.article) {
                        this.setState({
                            article: res.data.article,
                            status: "waiting"
                        });
                        // Subir archivo
                        if (this.state.selectedFile !== null) {
                            // Id de articulo
                            var id = this.state.article._id;

                            // Form data
                            const formData = new FormData();
                            formData.append("file0", this.state.selectedFile,
                                this.state.selectedFile.name);
                            // peticion ajax

                            Swal(
                                'Artículo creado',
                                'Ola k ase',
                                'success'
                            );

                            Axios.post(this.url + "upload-image/" + id, formData)
                                .then(res => {
                                    console.log(res.data);
                                    if (res.data.article) {
                                        this.setState({
                                            article: res.data.article,
                                            status: "success"
                                        });
                                    } else {
                                        this.setState({
                                            article: res.data.article,
                                            status: "error"
                                        });
                                    }
                                });

                        } else {
                            this.setState({
                                status: "success"
                            });
                            Swal(
                                'Artículo creado',
                                'Ola k ase sin foto o k ase',
                                'success'
                            );
                        }

                    } else {
                        this.setState({
                            status: "error"
                        });
                        Swal(
                            'Error',
                            'no se creo el articulo o k ase',
                            'errpr'
                        );
                    }
                },
                error =>{
                    console.log(error);
                });
        } else {
            this.setState({
                status: "error"
            });
        }
    }

    changeFile = (e) => {
        this.setState({
            selectedFile: e.target.files[0]
        });

    }

    render() {

        if (this.state.status === "success") {
            return (
                <Redirect to={"/blog/"} />
            );
        }
        var article = this.state.article;
        return (
            <div id="main">
                <div className="center">
                    <section id="content">
                        <h1 className="subheader">Editar artículo</h1>
                        {
                            this.state.article &&

                            <form className="mid-form" onSubmit={this.saveArticle} >
                                <div className="form-group">
                                    <label htmlFor="title">Título</label>
                                    <input type="text" name="title" id="" defaultValue={article.title} onChange={this.changeState} ref={this.titleRef} />
                                    {/* VALIDADOR */}
                                    {this.validator.message('title', article.title, 'required|alpha_num_space')}
                                </div>


                                <div className="form-group">
                                    <label htmlFor="content">Contenido</label>
                                    <textarea name="content" id="" defaultValue={article.content} onChange={this.changeState} ref={this.contentRef}></textarea>
                                    {this.validator.message('content', article.content, 'required')}

                                </div>

                                <br />

                                <div className="form-group">
                                    {/* <label htmlFor="file0">Contenido</label> */}
                                    <div className="image-wrap" >
                                            {
                                                article.image !== null ? (
                                                    <img className="image-uploader" src={this.url + 'get-image/' + article.image} alt={article.title} />
                                                ) :
                                                    (
                                                        <img className="image-uploader" src={ImageDefault} alt={article.title} />
                                                    )
                                            }

                                        </div>
                                    <input type="file" name="file0" id="" onChange={this.changeFile} ref={this.fileRef} />
                                </div>

                                <br />

                                <div className="clearfix"></div>
                                <input type="submit" value="Enviar" className="btn btn-success" />
                            </form>
                        }
                        {!this.state.article &&
                            <h1>Cargando...</h1>
                        }
                    </section>
                    <Sidebar></Sidebar>
                </div>
            </div>

        );
    }
}

export default EditArticle;