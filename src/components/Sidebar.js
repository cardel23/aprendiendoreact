import React, { Component, Fragment } from "react";
import { Redirect, Link } from "react-router-dom";

class Sidebar extends Component {

    searchRef = React.createRef();

    state = {
        search: "",
        redirect: false
    };

    redirectSearch = (e) => {
        e.preventDefault();
        var value = this.searchRef.current.value;
        console.log(value);
        if(value !== "")
        this.setState({
            search: this.searchRef.current.value,
            redirect: true
        });
    };

    render() {
        if (this.state.redirect) {
            return (
                <Redirect to={"/redirect/" + this.state.search} />
            );
        }
        return (

            <Fragment>
                <aside id="sidebar">
                    <div id="nav-blog" className="sidebar-item">
                        <h3>Puedes hacer esto</h3>
                        {/* <a href="/article" className="btn btn-success">Crear articulo</a> */}
                        <Link to="/blog/new" className="btn btn-success">Crear articulo</Link>
                    </div>
                    <div id="search" className="sidebar-item">
                        <h3>Buscador</h3>
                        <p>Encuentra el articulo que buscas</p>
                        <form onSubmit={this.redirectSearch} action="">
                            <input type="text" name="search" id="search" ref={this.searchRef} />
                            <input className="btn" type="submit" name="submit" value="Buscar" id="" />
                        </form>
                    </div>
                </aside>
                <div className="clearfix"></div>
            </Fragment>
        );
    }
}

export default Sidebar;