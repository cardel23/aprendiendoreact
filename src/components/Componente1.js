import React, { Component } from 'react';

class Componente1 extends Component {
	// Obligado
	/**
	 * Es el metodo que muestra las vistas y tiene que tener un return
	 */
	render() {

		let receta = {
			nombre: 'Pizza',
			ingredientes: ['Tomate', 'Queso', 'Jamon'],
			calorias: 550
		};

		return (
			// Dentro del return solo puede haber una etiqueta padre
			// por eso usa <React.Fragment>
			<React.Fragment>
				<h1>{receta.nombre}</h1>
				<h2>{'Calorias: ' + receta.calorias}</h2>
				{
					this.props.saludo &&
					<h3>{this.props.saludo}</h3>
				}
		
				<ol>
					{
						receta.ingredientes.map(function(ingrediente, i) {
							// console.log(ingrediente);
							return (
								<li key={i}>
									{ingrediente}
								</li>
							);
						})
					}
				</ol>
				<hr />
			</React.Fragment>
		);
	}
}

export default Componente1;