import React, { Component } from "react";
import Slider from "./Slider";
import Sidebar from "./Sidebar";

import axios from 'axios';
import Articles from "./Articles";

class Blog extends Component {

    state = {
        articles: [],
        status: ""
    };

    componentDidUpdate(prevProps, prevState) {
        if (prevState.status !== this.state.status) {
            axios.get("http://localhost:3900/api/articles")
            .then(response => {
                // console.log(response.data);
                this.setState({
                    articles: response.data.articles,
                    status: "success"
                });
            });
        }
    }

    render() {

        // NO HACERLO AQUI PORQUE SE HACEN PETICIONES INFINITAS
        // axios.get("http://localhost:3900/api/articles")
        //     .then(response => {
        //         // console.log(response.data);
        //         this.setState({
        //             articles: response.data.articles
        //         });
        //     });
        // console.log(this.props)
        return (
            <div id="main" className="">
                <Slider size="slider-small"
                    title="Blog" />

                <div className="center">

                    <section id="content">

                        <h1 className="subheader">Blog</h1>
                        {/* Lista de articulos del api  */}
                        <Articles/>
                    </section>
                    <Sidebar />

                    <div className="clearfix"></div>

                </div>

            </div>
        );
    }

}

export default Blog;