import React, { Component } from "react";

import Componente1 from "./Componente1";
// import Peliculas from './Peliculas';





class SeccionPruebas extends Component {

    contador = 0;

    /**
     * Para definir el estado de las variables hay que crear un constructor
     * si no las variables en pantalla no cambian su estado como en angular
     * @param {*} props 
     */
    constructor(props) {
        super(props);

        this.state = {
            contador: 0
        };

    }
    /**
     * Tambien se puede solo definiendo el objeto state
     * @param {*} nombre 
     */
    // state = {
    //     contador: 0
    // };


    // Las funciones no se definen con function
    HolaMundo(nombre) {
        var presentacion = <h4>Ola k ase soy {nombre} o k ase</h4>
        return presentacion;
    }


    /**
     * Para modificar el estado de las variables se usa el metodo setState
     * Dentro de los parametros se indica un JSON con el nombre de la variable
     * y el nuevo estado como valor
     */
    sumar(e) {
        // e ++;
        // this.contador += 1;
        // this.state.contador += 1;
        this.setState({
            // eslint-disable-next-line
            contador: this.state.contador += 1
        })
    }


    restar = (e) => {
        this.setState({
            // eslint-disable-next-line
            contador: this.state.contador -= 1
        })
    }

    render() {

        var nombre = "Carlos";
        return (
            <div id='main'>
                <section id="content">
                    <h2 className="subheader">Ultimos articulos</h2>
                    <p>
                        Edit <code>src/App.js</code> and save to reload.
                    </p>

                    <h2 className="subheader">Funciones y JSX básico</h2>

                    {this.HolaMundo(nombre)}

                    <section className="componentes">


                        <Componente1></Componente1>


                    </section>
                    {/* <Peliculas></Peliculas> */}

                    <h2 className="subheader">Estado</h2>
                    <p>
                        {/* {this.contador} */}
                        {/* Se usa la variable definida en state */}
                        {"Contado: " + this.state.contador}
                    </p>
                    <p>
                        <input type="button" onClick={this.sumar.bind(this)} value="Sumar"></input>
                        <input type="button" onClick={this.restar} value="Restar"></input>
                    </p>

                </section>
            </div>
        );
    }
}

export default SeccionPruebas;