import React, { Component } from "react";

class Pelicula extends Component {


    // Hay que hacer un metodo aparte para pasar parametros a un metodo
    // desde el componente padre
    fav = () => {
        this.props.fav(this.props.pelicula, this.props.index);
    }

    render() {
        // const pelicula = this.props.pelicula;
        const { title, image } = this.props.pelicula;
        
        return (
            <article className="article-item" id="article-template">
                <div className="image-wrap">
                    <img src={image} alt={title} />

                </div>

                <h2 className="">
                    {title}
                </h2>

                <span className="date" >
                    Hace 5 minutos
                </span>

                <a href="/">Leer mas</a>

                <button onClick={this.fav}>
                    Favorita
                </button>

                <div className="clearfix"></div>
            </article>
        )
    }
}

export default Pelicula;