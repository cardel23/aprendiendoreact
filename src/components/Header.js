import React, { Component, Fragment } from 'react';
import logo from "../assets/images/logo.svg";
import { NavLink } from 'react-router-dom';

class Header extends Component {
    render() {
        return (
            <Fragment>

                <header id="header">
                    <div className="center">
                        {/* <!--logo--> */}
                        <div id="logo">
                            <img src={logo} className="app-logo" alt="Logotipo" />
                            <span id="brand">
                                <strong>Curso</strong>React
                    </span>
                        </div>
                        {/* <!--menu--> */}
                        <nav id="menu">
                            <ul>
                                <li>
                                    {/* <a href="index.html">Inicio</a> */}
                                    <NavLink to="/home" activeClassName="active" >Inicio</NavLink>
                                </li>
                                <li>
                                    <NavLink to="/blog">Blog</NavLink>
                                </li>
                                <li>
                                    <NavLink to="/formulario">Formulario</NavLink>
                                </li>
                                <li>
                                    <NavLink to="/peliculas">Peliculas</NavLink>
                                </li>
                                <li>
                                    <NavLink to="/pagina">Pagina</NavLink>
                                </li>
                            </ul>
                        </nav>
                        {/* <!--limpia float--> */}
                        <div className="clearfix">

                        </div>
                    </div>
                </header>
            </Fragment>
        );
    }
}

export default Header;