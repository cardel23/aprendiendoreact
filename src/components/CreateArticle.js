import React, { Component } from "react";
import Axios from "axios";
import Global from "../Global";
import Sidebar from "./Sidebar";
import { Redirect } from "react-router-dom";
import SimpleReactValidator from 'simple-react-validator';
import Swal from 'sweetalert';

class CreateArticle extends Component {

    url = Global.url;

    titleRef = React.createRef();
    contentRef = React.createRef();
    fileRef = React.createRef();

    state = {
        article: {},
        status: null,
        selectedFile: null
    };

    constructor() {
        super();
        this.validator = new SimpleReactValidator({
            // errores para cada campo segun la condicion
            messages: {
                required: "Este campo es requerido",
                alpha_num_space: "El título no puede tener caracteres especiales"
            }
        });
    }

    changeState = () => {
        this.setState({
            article: {
                title: this.titleRef.current.value,
                content: this.contentRef.current.value
            }
        });
        this.validator.showMessages();
        this.forceUpdate();
        // console.log(this.state.article);
    };


    // NO PONER PARENTESIS AL SUBMIT SINO NO FUNCIONA 
    saveArticle = (e) => {
        e.preventDefault();
        this.changeState();
        if (this.validator.allValid()) {
            Axios.post(this.url + "/save", this.state.article)
                .then(res => {
                    console.log(res.data);
                    if (res.data.articleStored) {
                        console.log(res.data);
                        this.setState({
                            article: res.data.articleStored,
                            status: "waiting"
                        });
                        // Subir archivo
                        if (this.state.selectedFile !== null) {
                            // Id de articulo
                            var id = this.state.article._id;

                            // Form data
                            const formData = new FormData();
                            formData.append("file0", this.state.selectedFile,
                                this.state.selectedFile.name);
                            // peticion ajax

                            Swal(
                                'Artículo creado',
                                'Ola k ase',
                                'success'
                            );

                            Axios.post(this.url + "upload-image/" + id, formData)
                                .then(res => {
                                    console.log(res.data);
                                    if (res.data.article) {
                                        this.setState({
                                            article: res.data.article,
                                            status: "success"
                                        });
                                    } else {
                                        this.setState({
                                            article: res.data.article,
                                            status: "error"
                                        });
                                    }
                                });
                                
                        } else {
                            this.setState({
                                status: "success"
                            });
                            Swal(
                                'Artículo creado',
                                'Ola k ase sin foto o k ase',
                                'success'
                            );
                        }

                    } else {
                        this.setState({
                            status: "error"
                        });
                        Swal(
                            'Error',
                            'no se creo el articulo o k ase',
                            'errpr'
                        );
                    }
                });
        } else {
            this.setState({
                status: "error"
            });
        }
    }

    changeFile = (e) => {
        this.setState({
            selectedFile: e.target.files[0]
        });

    }

    render() {

        if (this.state.status === "success") {
            return (
                <Redirect to={"/blog/"} />
            );
        }

        return (
            <div id="main">
                <div className="center">
                    <section id="content">
                        <h1 className="subheader">Nuevo artículo</h1>
                        <form className="mid-form" onSubmit={this.saveArticle} >
                            <div className="form-group">
                                <label htmlFor="title">Título</label>
                                <input type="text" name="title" id="" onChange={this.changeState} ref={this.titleRef} />
                                {/* VALIDADOR */}
                                {this.validator.message('title', this.state.article.title, 'required|alpha_num_space')}
                            </div>


                            <div className="form-group">
                                <label htmlFor="content">Contenido</label>
                                <textarea name="content" id="" onChange={this.changeState} ref={this.contentRef}></textarea>
                                {this.validator.message('content', this.state.article.content, 'required')}

                            </div>

                            <br />

                            <div className="form-group">
                                {/* <label htmlFor="file0">Contenido</label> */}
                                <input type="file" name="file0" id="" onChange={this.changeFile} ref={this.fileRef} />
                            </div>

                            <br />

                            <div className="clearfix"></div>
                            <input type="submit" value="Enviar" className="btn btn-success" />
                        </form>
                    </section>
                    <Sidebar></Sidebar>
                </div>
            </div>

        );
    }
}

export default CreateArticle;