import React, { Component } from "react";
import Slider from "./Slider";
import Sidebar from "./Sidebar";
import Articles from "./Articles";

class Search extends Component {

    render() {
        var search = this.props.match.params.search;
        return (
            <div id="main" className="">
                <Slider size="slider-small"
                    title={"Busqueda: " + search} />

                <div className="center">

                    <section id="content">

                        <h1 className="subheader">Blog</h1>
                        {/* Lista de articulos del api  */}
                        <Articles search={search} />
                    </section>
                    <Sidebar
                    />


                </div>

            </div>
        )
    }

}

export default Search;